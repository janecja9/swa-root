# SWA - Root

## Diagram of app architecture

![](docs/architecture.png)

## Services

Externally, the bookmaker system can be accessed via two services. The first is the **odds-service**, which can be used to list all posted odds. The second is the **bookmaker-service**, through which individual users can place bets on selected odds.

Internally, the **bookmaker-service** communicates with the **odds-service** and **user-service** to verify the existence and correctness of bets and users, respectively.

Bookmaker service - https://gitlab.fel.cvut.cz/janecja9/swa-bookmaker-service

Odds service - https://gitlab.fel.cvut.cz/janecja9/swa-odds-service

User service - https://gitlab.fel.cvut.cz/janecja9/swa-user-service

Service register - https://gitlab.fel.cvut.cz/janecja9/swa-register-service

## Semestral work tasks

1. Codebase – one codebase tracked in revision control, many deploys
   1. [x] separate repositories for each microservice (1)
   2. [ ] continuous integration to build, test and create the artifact (3)
   3. [ ] implement some tests and test each service separately (unit tests, integration tests) (5)
2. Dependencies – explicitly declare and isolate dependencies
   1. [ ] preferably Maven project with pom.xml
   2. [ ] eventually gradle project or other
3. Config
   1. [ ] configuration of services provided via environmental properties (1)
   2. [ ] eventually as configuration as code (bonus: 0.5)
4. Backing services – treat backing services as attached resources
   1. [ ] backing services like database and similar will be deployed as containers too (1)
5. Build, release, run – strictly separate build and run stages
   1. [ ] CI & docker
   2. [ ] eventually upload your images to docker hub (bonus: 1)
6. [ ] Processes – execute the app as one or more stateless processes (1)
7. [ ] Port binding – export services via port binding (1)
8. Disposability – maximize robustness with fast startup and graceful shutdown
   1. [ ] ability to stop/restart service without catastrophic failure for the rest (2)
9. Dev/prod parity – keep development, staging, and production as similar as possible
   1. [ ] 3-5 integration tests (5)
   2. [ ] services will be deployed as containers (2)
10. Logs – treat logs as event streams
    1. [ ] log into standard output (1)
    2. [ ] eventually collect logs in Elastic (bonus: 0.5)
11. Communication
    1. [ ] REST API defined using Open API standard (Swagger) (2)
    2. [ ] auto-generated in each service (1)
    3. [ ] clear URLs (2)
    4. [ ] clean usage of HTTP statuses (2)
    5. [ ] eventually message based asynchronous communication via queue (bonus: 1)
12. Transparency – the client should never know the exact location of a service.
    1. [ ] service discovery (2)
    2. [ ] eventually client side load balancing (bonus: 0.5) or workload balancing (bonus: 0.5)
13. Health monitoring – a microservice should communicate its health
    1. [ ] Actuators (1)
    2. [ ] eventually Elastic APM (bonus: 1)
14. [ ] Design patterns – use the appropriate patterns (2)
15. [ ] Scope – use domain driven design or similar to design the microservices (5)
16. [x] Documentation – visually communicate architecture of your system (5)
    1. https://c4model.com/ (https://github.com/RicardoNiepel/C4-PlantUML)
